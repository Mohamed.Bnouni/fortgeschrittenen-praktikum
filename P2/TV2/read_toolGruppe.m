close all;
clear all;
serial_number='34801403'; %Polaris serial number

data_file='TV2_after_004.tsv';
%read file
S=importdata(data_file,'\t');
%...GET TOOL...........................................
n_frame=zeros(size(S.textdata,1)-1,2);
data=NaN*ones(size(S.textdata,1)-1,3);
err=NaN*ones(size(S.textdata,1)-1,1);
n_tools=str2double(S.textdata(2,1));
% skip_cells=1+16*(n_tools-1)+13;
format_header='%s';
for k=1:size(S.textdata,2)-1
 format_header=strcat(format_header,'%s');
end
format_data=format_header;
for k=1:n_tools
 format_data=strcat(format_data,'%s%s%s%s');
end
FID = fopen(data_file);
H = textscan(FID,format_header,1); %read the header
ind_tool=[];
for k=1:length(H)
 if strcmp(H{k},'Tx')==1
 ind_tool=[ind_tool;k];
 end
end
temp=fgetl(FID); %empty line
for i=2:size(S.textdata,1) %potential correct frames (from 2 to end of file)
 temp=str2double(S.textdata(i,3));
 if mod(temp,1)==0 %check if the frame number is an integer value (valid frames)
 n_frame(i-1,1)=temp;
% temp=textscan(FID,format_data,1);
 temp=fgetl(FID); %empty line
 temp2=strsplit(temp);
 if strcmp(temp2{9},'OK')==1 %check if tool has been detected
 data(i-1,1)=str2num(temp2{ind_tool(1)}); %13 to 16
 data(i-1,2)=str2num(temp2{ind_tool(1)+1});
 data(i-1,3)=str2num(temp2{ind_tool(1)+2});
 err(i-1,1)=str2num(temp2{ind_tool(1)+3});
 else
 n_frame(i-1,1)=NaN;
 end
 else
 n_frame(i-1,1)=NaN;
 end
34
end
fclose(FID);
...END (GET TOOL)......................................
 
%PLOT
close all;
plot3(data(:,1),data(:,2),data(:,3),'.');
xlabel('X');
ylabel('Y');
zlabel('Z');
axis equal;
ok_err=find(~isnan(err));
if ~isempty(ok_err)
 figure
 hist(err);
end
